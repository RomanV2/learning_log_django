from django.db import models


class Topic(models.Model):
    """
    Тема, изучаемая пользователем.
    """
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """
        возврат модели строкой
        """
        return self.text


class Entry(models.Model):
    """
    информация пользователя по конкретной теме.
    """
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        """
        Доп информация по управлению модели.
        при обращении более чем к одной записи использовать форму множественного числа Entries
        """
        verbose_name_plural = 'entries'

    def __str__(self):
        """
        возврат модели строкой
        """
        if len(self.text) > 50:
            return f"{self.text[0:50]}..."
        else:
            return f"{self.text}"
